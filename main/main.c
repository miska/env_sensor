#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"
#include "esp_netif.h"
#ifdef CONFIG_DEEP_SLEEP
#include "esp_sleep.h"
#endif

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "dht.h"
#include "ds18x20.h"
#include "bmp180.h"

#include "esp_tls.h"
#include "esp_crt_bundle.h"

/* Constants that aren't configurable in menuconfig */
#define WEB_URL "https://" CONFIG_INFLUXDB_HOST "/api/v2/write?org=" CONFIG_INFLUXDB_ORG "&bucket=" CONFIG_INFLUXDB_BUCKET "&precision=s"
#define WEB_PORT "443"

#define MSG_MAX_LEN 8192
#define MSG_LINE_MAX_LEN 2048

static char msgbuff[MSG_MAX_LEN];
static char linebuff[MSG_LINE_MAX_LEN];

static const char *TAG = "env_sensor";

static const char *REQUEST = "POST /api/v2/write?org=" CONFIG_INFLUXDB_ORG "&bucket=" CONFIG_INFLUXDB_BUCKET " HTTP/1.1\r\n"
                             "Host: " CONFIG_INFLUXDB_HOST "\r\n"
                             "Content-Type: text/plain\r\n"
                             "Content-Length: %d\r\n"
                             "Authorization: Token " CONFIG_INFLUXDB_TOKEN "\r\n"
                             "User-Agent: esp-idf/1.0 esp32\r\n"
                             "\r\n%s";

static void upload_data() {
    // Prepare request to be send
    int ret, len;

    // Init TLS
    esp_tls_cfg_t cfg = {
        .crt_bundle_attach = esp_crt_bundle_attach,
    };
    esp_tls_t *tls = esp_tls_conn_http_new(WEB_URL, &cfg);
    if(tls != NULL) {
        ESP_LOGI(TAG, "Connection established...");
    } else {
        ESP_LOGE(TAG, "Connection failed...");
        goto upload_exit;
    }

    // Send the data
    size_t written_bytes = 0;
    do {
        ret = esp_tls_conn_write(tls,
                                 msgbuff + written_bytes,
                                 strlen(msgbuff) - written_bytes);
        if(ret >= 0) {
            ESP_LOGI(TAG, "%d bytes written", ret);
            written_bytes += ret;
        } else if(ret != ESP_TLS_ERR_SSL_WANT_READ  && ret != ESP_TLS_ERR_SSL_WANT_WRITE) {
            ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
            goto upload_exit;
        }
    } while(written_bytes < strlen(msgbuff));

    // Read response
    ESP_LOGI(TAG, "Reading HTTP response...");
    do {
        len = sizeof(msgbuff) - 1;
        bzero(msgbuff, sizeof(msgbuff));
        ret = esp_tls_conn_read(tls, (char *)msgbuff, len);

        if(ret == ESP_TLS_ERR_SSL_WANT_WRITE  || ret == ESP_TLS_ERR_SSL_WANT_READ)
            continue;

        if(ret < 0) {
            ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
            break;
        }

        if(ret == 0) {
            ESP_LOGI(TAG, "connection closed");
            break;
        }

        len = ret;
        ESP_LOGD(TAG, "%d bytes read", len);
        /* Print response directly to stdout as it is read */
        for(int i = 0; i < len; i++) {
            putchar(msgbuff[i]);
        }
    } while(1);

upload_exit:
    if(tls != NULL)
        esp_tls_conn_delete(tls);
}

static int ds18x20_sample() {
    static ds18x20_addr_t addr = 0;
    float temp;
    esp_err_t res;
    size_t sensor_count = 0;

    // First run
    if(addr == 0 || sensor_count == 0) {
        gpio_set_pull_mode(CONFIG_DS18X20_GPIO, GPIO_PULLUP_ONLY);
        res = ds18x20_scan_devices(CONFIG_DS18X20_GPIO, &addr, 1, &sensor_count);
        if(res != ESP_OK || sensor_count == 0) {
            ESP_LOGE("ds18x20_task", "No sensor detected!");
            return res;
        }
    }

    res = ds18x20_measure_and_read(CONFIG_DS18X20_GPIO, addr, &temp);
    if(res != ESP_OK) {
        ESP_LOGE("ds18x20_task", "Reading from sensor failed!");
        return res;;
    }

    ESP_LOGI("ds18x20_task", "Got temperature (%.2f)", temp);
    snprintf(linebuff, MSG_LINE_MAX_LEN,
             "temperature" CONFIG_INFLUXDB_TAGS " value=%.2f", temp);
    return ESP_OK;
}

static int dht22_sample() {
    int16_t t, h;
    esp_err_t res;

    if((res = dht_read_data(DHT_TYPE_AM2301, CONFIG_DHT22_GPIO, &h, &t)) != ESP_OK) {
        ESP_LOGE("dht22_task", "Reading temperature failed...");
        return res;
    }

    ESP_LOGI("dht11_task", "Got temperature (%d) and humidity (%d)", t, h);
    snprintf(linebuff, MSG_LINE_MAX_LEN,
             "temperature" CONFIG_INFLUXDB_TAGS " value=%d.%d\n"
             "humidity" CONFIG_INFLUXDB_TAGS " value=%d.%d\n",
             t / 10, t % 10, h / 10, h % 10);
    return ESP_OK;
}

static int dht11_sample() {
    int16_t t, h;
    esp_err_t res;

    if((res = dht_read_data(DHT_TYPE_DHT11, CONFIG_DHT11_GPIO, &h, &t)) != ESP_OK) {
        ESP_LOGE("dht11_task", "Reading temperature failed...");
        return res;
    }

    ESP_LOGI("dht11_task", "Got temperature (%d) and humidity (%d)", t, h);
    snprintf(linebuff, MSG_LINE_MAX_LEN,
             "temperature" CONFIG_INFLUXDB_TAGS " value=%d.%d\n"
             "humidity" CONFIG_INFLUXDB_TAGS " value=%d.%d\n",
             t / 10, t % 10, h / 10, h % 10);
    return ESP_OK;
}

static int bmp180_sample() {
    static bmp180_dev_t dev = {};
    float temp;
    uint32_t pressure;
    esp_err_t res;

    if(dev.i2c_dev.addr == 0) {
        ESP_ERROR_CHECK(bmp180_init_desc(&dev, 0, CONFIG_BMP180_SDA_GPIO, CONFIG_BMP180_SCL_GPIO));
        ESP_ERROR_CHECK(bmp180_init(&dev));
    }

    res = bmp180_measure(&dev, &temp, &pressure, BMP180_MODE_HIGH_RESOLUTION);
    if(res != ESP_OK) {
        ESP_LOGE("bmp180_task", "Error reading sensor!");
        return res;
    }
    ESP_LOGI("bmp_task", "Got temperature (%.2f) and pressure (%d)", temp, pressure);
    snprintf(linebuff, MSG_LINE_MAX_LEN,
             "temperature" CONFIG_INFLUXDB_TAGS " value=%.2f\n"
             "pressure" CONFIG_INFLUXDB_TAGS " value=%d\n",
             temp, pressure);
    return ESP_OK;
}

static void measure_task() {
    while(1) {
        strncpy(msgbuff, REQUEST, MSG_MAX_LEN);
        if(CONFIG_DS18X20_GPIO > 0) {
            if(ds18x20_sample() == ESP_OK)
                strncat(msgbuff, linebuff, MSG_MAX_LEN - strlen(msgbuff));
        }
        if(CONFIG_BMP180_SDA_GPIO > 0 && CONFIG_BMP180_SCL_GPIO > 0) {
            if(bmp180_sample() == ESP_OK)
                strncat(msgbuff, linebuff, MSG_MAX_LEN - strlen(msgbuff));
        }
        if(CONFIG_DHT22_GPIO > 0) {
            if(dht22_sample() == ESP_OK)
                strncat(msgbuff, linebuff, MSG_MAX_LEN - strlen(msgbuff));
        }
        if(CONFIG_DHT11_GPIO > 0) {
            if(dht11_sample() == ESP_OK)
                strncat(msgbuff, linebuff, MSG_MAX_LEN - strlen(msgbuff));
        }
        upload_data();
#ifdef CONFIG_DEEP_SLEEP
        ESP_LOGI("main_task", "Going to sleep..");
        ESP_ERROR_CHECK(example_disconnect());
        esp_sleep_enable_timer_wakeup(CONFIG_SAMPLE_INTERVAL * 1000);
        esp_deep_sleep_start();
        ESP_LOGI("main_task", "Waking up..");
        ESP_ERROR_CHECK(example_connect());
#else
        vTaskDelay(CONFIG_SAMPLE_INTERVAL * 1000 / portTICK_PERIOD_MS);
#endif
    }
}

void app_main(void) {
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    ESP_ERROR_CHECK(example_connect());
    measure_task();
    ESP_ERROR_CHECK(example_disconnect());
}
